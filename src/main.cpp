#include <Quadrado.hpp>
#include <iostream>
#include <Forma.hpp>
#include <ctype.h>
#include <Barra.hpp>
#include <Nave1.hpp>
#include <Nave2.hpp>
#include <Glider.hpp>
#include <Campo.hpp>
#include <Gosper_Glider_Gun.hpp>
using namespace std;
void mostrarMatriz(char** campo,int dimx,int dimy){
	for(int i=0;i<dimx;i++){
		for(int j=0;j<dimy;j++){
			cout<<campo[i][j];
		}
	cout<<endl;
	}
}
void aplicarRegras(char**campo,int dimx, int dimy){
int **vizinhos;
vizinhos=new int*[dimx];
for(int i=0;i<dimx;i++){
	vizinhos[i]=new int[dimy];
}
for(int i=0;i< dimx;i++){
	for(int j=0;j< dimy;j++){
	vizinhos[i][j]=0;
	}
}
	for(int i=1;i< dimx-1;i++){
		for(int j=1;j< dimy-1;j++){
				if(campo[i-1][j-1]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i-1][j]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i-1][j+1]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i][j-1]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i][j+1]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i+1][j-1]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i+1][j]=='*'){
					vizinhos[i][j]++;
				}
				if(campo[i+1][j+1]=='*'){
					vizinhos[i][j]++;
				}
		}
	}
for(int i=0;i<dimx;i++){
		for(int j=0;j<dimy;j++){
			if(vizinhos[i][j]<2|| vizinhos[i][j]>3){
				campo[i][j]=' ';
			}
			if(vizinhos[i][j]==3){
				campo[i][j]='*';
			}
		}
}
	for(int i=0;i<dimx;i++){
		delete[]vizinhos[i];
	}
	delete[]vizinhos;
}

int main(){
int geracao=0;
char  resposta;
int  resposta2;
Barra    b;
Glider   g;
Quadrado q;
Nave1   n1;
Nave2   n2;
Gosper_Glider_Gun ggg;
cout<<"=========================================================================================================="<<endl;
cout<<"= Copyright (c) by Victor Hugo Dias Coelho.                                                              ="<<endl;
cout<<"= Copyright (c) 2017 Copyright Holder All Rights Reserved.                                               ="<<endl;
cout<<"= JOGO DA VIDA:                                                                                          ="<<endl;
cout<<"= Regras:                                                                                                ="<<endl;
cout<<"= 1-Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.                              ="<<endl;
cout<<"= 2-Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.                        ="<<endl;
cout<<"= 3-Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.                   ="<<endl;
cout<<"= 4-Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geracao.="<<endl;
cout<<"=========================================================================================================="<<endl;
Campo campo1;
inicio:
cout<<"==============================="<<endl;
cout<<"= Digite o numero de geracoes ="<<endl;
cout<<"==============================="<<endl;
scanf("%d",&geracao);
if(!geracao){
cout<<"====================="<<endl;
cout<<"= Caracter invalido ="<<endl;
cout<<"====================="<<endl;
getchar();
system("clear");
goto inicio;
}


while(1){
		cout<<"================================"<<endl;
	  cout<<"= deseja adicionar forma(S/N)? ="<<endl;
		cout<<"================================"<<endl;
		cin>>resposta;
		resposta=toupper(resposta);
	if(resposta == 'S'){
		inicio2:
		cout<<"=================================="<<endl;
		cout<<"= Que figura deseja adicionar?   ="<<endl;
		cout<<"= 1- Barra  2- Quadrado          ="<<endl;
		cout<<"= 3- Glider 4- Nave1             ="<<endl;
		cout<<"= 5- Nave2  6- Gosper Glider Gun ="<<endl;
		cout<<"=================================="<<endl;
		scanf("%d",&resposta2);
		if(!resposta2){
		cout<<"====================="<<endl;
		cout<<"= Caracter invalido ="<<endl;
		cout<<"====================="<<endl;
		getchar();
		system("clear");
		goto inicio2;
		}
		switch (resposta2){
		case 1:
			b.aplicarbarra(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		case 2:
			q.aplicarquadrado(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		case 3:
			g.aplicarglider(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		case 4:
			n1.aplicarnave1(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		case 5:
			n2.aplicarnave2(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		case 6:
			ggg.aplicargosperglidergun(campo1.campo,campo1.dimx,campo1.dimy);
			break;
		}
	}
	else{
		cout<<endl;
		break;
	}
}
while(geracao){
	geracao--;
	aplicarRegras(campo1.campo,campo1.dimx,campo1.dimy);
	mostrarMatriz(campo1.campo,campo1.dimx,campo1.dimy);
	for(int i=0;i<15799778;i++){
	}
	system("clear");
	cout<<endl;
}
return 0;
}
