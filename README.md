1. Gosper Glider Gun - Victor Hugo Dias Coelho 16/0019401

  2.  Para executar o jogo abra o terminal na pasta em que vc extraiu o jogo, e escreva o seguinte comando "make run" de enter.

  3.  Após a execução do programa o usuário terá que digitar as dimensões do campo no qual ele deverá jogar, sendo que este terá que ser maior que ou igual 40X40 para que o jogo posso ter como executar todas suas funções.

  4.  Logo em seguida o usuário deve colocar o número de gerações que ele quer que seja executado o programa.

  5.  Nesta parte o individuo terá que falar se deseja adicionar uma forma ou não . Caso a resposta seja não o programa para de pedir mais informações ao usuário e executa as configurações do campo realizadas.

  6.  O próximo passo é colocar o número referente a forma que vc deseja colocar no campo. O número de formas que o usuário pode colocar é ilimitado !!!

  7.  Caso o usuário digite N na parte de adicionar forma o programa começa o jogo executando as regras no campo.

  Observação: Caso o usuário queira ver as formas disponiveis no jogo basta acessar o Formas.txt na pasta doc.
