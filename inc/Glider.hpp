#ifndef _GLIDER_HPP_
#define _GLIDER_HPP_
#include <iostream>
#include <Forma.hpp>

class Glider :public Forma {
protected:
  char** glider;
public:
  Glider();
  ~Glider();

  char** getGlider();
  void aplicarglider(char** campo,int dimx,int dimy);

};

#endif
