#ifndef _NAVE1_HPP_
#define _NAVE1_HPP_
#include<Forma.hpp>
#include<iostream>

class Nave1: public Forma {
protected:
  char**nave1;
public:
  Nave1();
  ~Nave1();

  char** getNave1();
  void aplicarnave1(char**campo,int dimx,int dimy);
};

#endif
