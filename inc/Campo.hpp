#ifndef _CAMPO_HPP_
#define _CAMPO_HPP_
#include<Forma.hpp>
#include<iostream>
using namespace std;

class Campo:public Forma{
public:
char** campo;
int dimx;
int dimy;

public:
	Campo();
	~Campo();
	char** getCampo();
	int getDimx();
	int getDimy();
};


#endif
