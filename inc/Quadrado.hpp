#ifndef _QUADRADO_HPP_
#define _QUADRADO_HPP_
#include <bits/stdc++.h>
#include <Forma.hpp>

class Quadrado : public Forma{
protected:

	char** quadrado;

public:
	Quadrado();
	~Quadrado();

	char** getQuadrado();
	void aplicarquadrado(char** campo,int dimx,int dimy);
};



#endif
