#ifndef _NAVE2_HPP_
#define _NAVE2_HPP_
#include<Forma.hpp>
#include<iostream>
using namespace std;
class Nave2: public Forma{
protected:
  char**nave2;
public:
  Nave2();
  ~Nave2();
  char** getNave2();
  void aplicarnave2(char**campo,int dimx,int dimy);
};

#endif
