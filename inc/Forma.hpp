#ifndef _FORMA_HPP_
#define _FORMA_HPP_

#include<bits/stdc++.h>
using namespace std;

class Forma{
protected:
	int dimensaox,dimensaoy;
public:
	Forma();
	~Forma();
	int getDimensaox();
	void setDimensaox(int dimensaox);
	int getDimensaoy();
	void setDimensaoy(int dimensaoy);
};

#endif
