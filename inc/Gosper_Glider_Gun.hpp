#ifndef _GOSPER_GLIDER_GUN_HPP
#define _GOSPER_GLIDER_GUN_HPP
#include <iostream>
#include <Forma.hpp>
using namespace std;

class Gosper_Glider_Gun : public Forma{
protected:
  char**gosperGliderGun;
public:
  Gosper_Glider_Gun();
  ~Gosper_Glider_Gun();

  char** getGosper_Glider_Gun();
  void aplicargosperglidergun(char**campo,int dimx,int dimy);
};

#endif
