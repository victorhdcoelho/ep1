#ifndef _BARRA_HPP_
#define _BARRA_HPP_
#include<bits/stdc++.h>
#include<Forma.hpp>

class Barra : public Forma{
protected:
	char** barra;

public:
	Barra();
	~Barra();

	char** getBarra();
	void aplicarbarra(char** campo, int dimx,int dimy);

};



#endif
